/* eslint-disable no-undef */
var query = "";
function getQuery(e){
  e.preventDefault();
  if(!$('#searchbox').val())
  {
    reloadPage();
  }
  
  query = $('#searchbox').val();
  getSearch(e, 1);
  
}
async function getData(event, page) {
  event.preventDefault();

  $('.card-columns').empty();
  $('.carousel .carousel-inner').empty();
  $('.pagination').empty();
  loading();

  const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=4545f05e9a363270fa164a4080bb8948&page=${page}`);
  const movies = await response.json();

  var i = 0;

  $('.carousel .carousel-inner').append(`<div class="carousel-item active" data-interval="5000" onclick="getDetails(${movies.results['5'].id})">
                                    <img src="https://image.tmdb.org/t/p/w1280${movies.results['5'].backdrop_path}" class="d-block w-100" alt="..." >
                                    <div class="carousel-caption d-none d-md-block" style="text-align: center;">
                                            <h1 style="font-weight:800;">${movies.results['5'].title}</h1>
                                            <h3> <span style="font-weight: 600;">${movies.results['5'].release_date}</span></h3>
                                            <br/>
                                    </div>
                                 </div>`)
  for (let item of movies.results) {


    if (i === 5) {
      break;
    }
    $('.carousel .carousel-inner').append(`<div class="carousel-item" data-interval="5000" onclick="getDetails(${movies.results[i.toString()].id})">
                                    <img src="https://image.tmdb.org/t/p/w1280${item.backdrop_path}" class="d-block w-100" alt="..." >
                                    <div class="carousel-caption d-none d-md-block" style="text-align: center;">
                                            <h1 style="font-weight:800;">${item.title}</h1>
                                            <h3> <span style="font-weight: 600;">${item.release_date}</span></h3>
                                            <br/>
                                    </div>
                                 </div>`)
    i++;
  }

  renderResults(movies, 1, page);

}

async function getSearch(e, page) {
  e.preventDefault();
  
  var temparr = query.split(' ');
  query = temparr.join('+');
  var selected = $('#search-mode :selected').text();
  temparr = query.split('+');
  var displayText = temparr.join(' ');

  $('body').empty();
  $('body').append(`<div class="spinner-border" style="top:50%; left:50%;display: block; position: fixed; z-index: 1031;" role="status">
  <span class="sr-only" style="top:50%; left:50%;"></span>
</div>`);



  if (selected === "Movie name") {
    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=4545f05e9a363270fa164a4080bb8948&query=${query}&page=${page}`);
    const movies = await response.json();

     
    $('body').empty();
    $('body').append(`<div class="container col-12 col-md-8"></div>`)
    $('.container').append(`<nav class="navbar navbar-light  my-2">
    <a class="btn btn-warning" onclick="reloadPage()">Home</a>
      <form class="form-inline" id='searchForm' submit="search(event, 1)" action="#">
      <input class="form-control mr-sm-1" id = 'searchbox' type="search" placeholder="Search" aria-label="Search">
      <select class="form-control form-control-md mr-sm-1" id="search-mode">
          <option>Movie name</option>
          <option>Actor's name</option>
        </select>
      <button class="btn btn-warning my-2 my-sm-0" type="submit" onclick="getQuery(event, 1); return false;" >Search</button>
      </form>
      </nav>
      <h1 class="my-5" style="text-align:center;">Search result for "${displayText}"</h1>
      <div class="card-columns my-5">
  
      </div>
  
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
  
        </ul>
      </nav>`);

    renderResults(movies, 2, page);
  } else {
    const response2 = await fetch(`https://api.themoviedb.org/3/search/person?api_key=4545f05e9a363270fa164a4080bb8948&query=${query}`);
    const moviesByActor = await response2.json();


    $('body').empty();
    $('body').append(`<div class="container col-12 col-md-8"></div>`)
    $('.container').append(`<nav class="navbar navbar-light  my-2">
    <a class="btn btn-warning" onclick="reloadPage()">Home</a>
      <form class="form-inline" id='searchForm' submit="search(event, 1)" action="#">
      <input class="form-control mr-sm-1" id = 'searchbox' type="search" placeholder="Search" aria-label="Search">
      <select class="form-control form-control-md mr-sm-1" id="search-mode">
          <option>Movie name</option>
          <option>Actor's name</option>
        </select>
      <button class="btn btn-warning my-2 my-sm-0" type="submit" onclick="getQuery(event, 1); return false;" >Search</button>
      </form>
      </nav>
      <h1 class="my-5" style="text-align:center;">Search result for "${displayText}"</h1>
      <div class="card-columns my-5">
  
      </div>
  
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
  
        </ul>
      </nav>`);

    $('ul').empty();
    $('.card-columns').empty();
    if(moviesByActor.total_pages==1 && moviesByActor.total_results!==0)
    {
      $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
    }
    else if(moviesByActor.total_pages==0)
    {

    }
    else
    {
      if (page === moviesByActor.total_pages) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
      } else if (page === 1) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${moviesByActor.total_pages}); return false;">Last</a></li>`);
      } else {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${moviesByActor.total_pages}); return false;">Last</a></li>`);
  
      }
    }
   

    for (let result of moviesByActor.results) {
      for (let movie of result['known_for']) {
        var imgSource = movie.poster_path;
        if (imgSource === null) {
          imgSource = "./resources/no-image.jpg";
        } else {
          imgSource = 'https://image.tmdb.org/t/p/w500' + movie.poster_path;
        }
        $('.card-columns').append(` <div class="card h-100 w-100 img-fluid border-0 text-center " onclick="getDetails(${movie.id})" >
        <img src="${imgSource}" class="card-img-top shadow" style="" alt="...">
        <div class="card-body ">
          <h5 class="card-title" style="text-align:left;" >${movie.title}</h5>
          <div class="row h-100">
            <div class="col-md-12"
              <p style="font-weight:100; text-align:center;" > <span style="font-weight:800; color: #ffd659;"> ${movie.vote_average}</span>  -  ${movie.vote_count} votes</p> 
            </div>
          </div>
        </div>
      </div>`);
      }
    }
  }

}

async function renderResults(movies, mode, page) {
  $('ul').empty();
  if (mode == 1) //Main mode
  {
    if(movies.total_pages===1 && movies.total_results!==0)
    {
      $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page}); return false;">${page}</a></li>`);
    }
    else if(movies.total_pages==0)
    {
      
    }
    else{
      if (page === movies.total_pages && page > 1) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page}); return false;">${page}</a></li>`);
      } else if (page === 1) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${movies.total_pages}); return false;">Last</a></li>`);
      } else {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getData(event,${movies.total_pages}); return false;">Last</a></li>`);
  
      }
    }
    
  } else //Search mode
  {
    if(movies.total_pages===1)
    {

      $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
    }
    else if(movies.total_pages==0)
    {
      
    }
    else{
      if (page === movies.total_pages && page > 1) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
      } else if (page === 1) {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${movies.total_pages}); return false;">Last</a></li>`);
      } else {
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${1}); return false;">First</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page-1}); return false;">Prev</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page}); return false;">${page}</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${page+1}); return false;">Next</a></li>`);
        $('ul').append(`<li class="page-item"><a class="page-link" href="javascript:" onclick="getSearch(event,${movies.total_pages}); return false;">Last</a></li>`);
  
      }
    }
    
  }

  
  $('.card-columns').empty();
  for (var movie in movies.results) {
    var imgSource = movies.results[movie].poster_path;
    if (imgSource === null) {
      imgSource = "./resources/no-image.jpg";
    } else {
      imgSource = 'https://image.tmdb.org/t/p/w500' + movies.results[movie].poster_path;
    }

    console.log(movies.results[movie]);
    $('.card-columns').append(` <div class="card h-100 w-100 img-fluid border-0 text-center " onclick="getDetails(${movies.results[movie].id})" >
    <img src="${imgSource}" class="card-img-top shadow" style="" alt="...">
    <div class="card-body ">
      <h5 class="card-title" style="text-align:center ;" >${movies.results[movie].title}</h5>
      <div class="row h-100">
        <div class="col-md-12"
          <p style="font-weight:100; text-align:center;" > <span style="font-weight:800; color: #ffd659;"> ${movies.results[movie].vote_average}</span>  -  ${movies.results[movie].vote_count} votes</p> 
        </div>
         
      </div>
    </div>
  </div>`);
  }
}

function loading() {
  $('.card-columns').append(`<div class="spinner-border" style="top:50%; left:50%;display: block; position: fixed; z-index: 1031;" role="status">
  <span class="sr-only" style="top:50%; left:50%;"></span>
</div>`);
}


async function getDetails(id) {

  $('body').empty();
  $('body').append(`<div class="spinner-border" style="top:50%; left:50%;display: block; position: fixed; z-index: 1031;" role="status">
  <span class="sr-only" style="top:50%; left:50%;"></span>
</div>`);
  const response = await fetch(`http://api.themoviedb.org/3/movie/${id}?api_key=4545f05e9a363270fa164a4080bb8948&append_to_response=credits`);
  const movie = await response.json();

  var genre = "";
  var tagline = movie.tagline;
  var director = "";

  for (let item of movie.genres) {
    genre = genre + ", ";
    genre = genre + item.name;
  }

  genre = genre.slice(1);

  if (!movie.tagline) {
    tagline = "No tag line available."
  }

  for (let item of movie.credits.crew) {
    if (item.job === "Director") {
      director = director + ", ";
      director = director + item.name;
    }
  }
  director = director.slice(1);




  $(window).scrollTop(0);
  $('body').empty();
  $('body').append(`<div class="container col-12 col-md-8"></div>`);

  $('.container').append(`<nav class="navbar navbar-light  my-2"  >
  <a class="btn btn-warning" onclick="reloadPage()">Home</a>
  <form class="form-inline" id='searchForm' submit="search(event, 1)" action="#">
    <input class="form-control mr-sm-1" id='searchbox' type="search" placeholder="Search" aria-label="Search">
    <select class="form-control form-control-md mr-sm-1" id="search-mode">
      <option>Movie name</option>
      <option>Actor's name</option>
    </select>
    <button class="btn btn-warning my-2 my-sm-0" type="submit"
      onclick="getQuery(event, 1); return false;">Search</button>
  </form>
</nav>`)
  $('body .container').append(`<img src="https://image.tmdb.org/t/p/w1280${movie.backdrop_path}" class="img-fluid mt-3 shadow-lg" alt="">
    <h1 class="text-center my-5">Overview</h1>
    <div class="card my-5 shadow-lg">
            <div class="row no-gutters">
              <div class="col-md-5 my-auto" >
                <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class=" card-img img-fluid my-auto" alt="..." >
              </div>
        
              <div class="col-md-7 my-auto">
                <div class="card-body mx-5 my-auto">
                  <h1 class="card-text" style="font-weight: 800;">${movie.title}</h5>
                  <p class="card-text">${movie.release_date}</p>
                  <h1 class="card-text" style="font-weight: 800; color: rgb(255, 166, 0);">${movie.vote_average}</h1>
                  <h5 class="card-text">${movie.runtime} minutes</h5>
                  <p class="card-text">${genre}</p>
                  <p style="font-style: italic;">"${tagline}"</p>
                  <h5 class="card-text">Directed by ${director}</h5>
                  <p class = "card-text text-justify" style="line-height: 1.8;">${movie.overview}</p>
                                            
                </div>
              </div>
            </div>
    </div>
    <h1 class="text-center mb-5">Cast</h1>

    <div id="carousel-cast" class="shadow-lg carousel carousel-fade" data-ride="carousel">

              <ol class="carousel-indicators" >
                  <li data-target="#carousel-cast" data-slide-to="0" class="active"></li>
              </ol>
              <div class="card border-0 shadow-lg carousel-inner text-center w-100 mx-auto my-auto" style="margin:0 auto;background-image: url('https://image.tmdb.org/t/p/w1280${movie.backdrop_path}');">
              </div>
              <a class="carousel-control-prev" href="#carousel-cast" role="button" data-slide="prev" >
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-cast" role="button" data-slide="next" >
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
      <h1 class="text-center my-5">Reviews</h1>
      <div class="row scrolling-wrapper my-5 " id="card-columns-secondary">

      </div>
        `);

  $('#carousel-cast .carousel-inner').empty();
  $('#carousel-cast .carousel-inner').append(`
  <div class="  my-5 mx-auto col-md-6 carousel-item active">
    <div class="row no-gutters">
      <div class="col-md-4 my-auto mx-auto">
        <img src="https://image.tmdb.org/t/p/w1280${movie.credits.cast['0'].profile_path}" class="shadow  card-img img-fluid" alt="...">
      </div>
    </div>
      
    <div class="row no-gutters">
      <div class="col-md-10 my-auto mx-auto">
        <div class="card-body mx-auto my-auto">
          <h2 class="card-title" style="font-weight: 800; color:white;">${movie.credits.cast['0'].name}</h2>
          <h5 class="card-title" style="font-weight: 800; color:white;"> <span style="font-weight: 400;">as</span> ${movie.credits.cast['0'].character}</h5>
          <button class="btn btn-outline-warning btn-md" type="button" onclick="getActor(${movie.credits.cast['0'].id})">Learn more</button>                         
        </div>
      </div>
    </div>
  </div>`);

  var pos = 1;
  for (let i = 1; i < movie.credits.cast.length; i++) {
    if (movie.credits.cast[i.toString()].character) {

      if (movie.credits.cast[i.toString()].profile_path === null) {
        continue;
      }
      $('.carousel-indicators').append(`<li data-target="#carousel-cast" data-slide-to="${pos}"></li>`)
      pos++;
      $('#carousel-cast .carousel-inner').append(`<div class="  my-5 mx-auto col-md-6 carousel-item ">
      <div class="row no-gutters">
        <div class="col-md-4 my-auto mx-auto">
          <img src="https://image.tmdb.org/t/p/w1280${movie.credits.cast[i.toString()].profile_path}" class="shadow card-img img-fluid" alt="...">
        </div>
      </div>
        
      <div class="row no-gutters">
        <div class="col-md-10 my-auto mx-auto">
          <div class="card-body mx-auto my-auto">
            <h2 class="card-title" style="font-weight: 800; color:white;">${movie.credits.cast[i.toString()].name}</h2>
            <h5 class="card-title" style="font-weight: 800; color:white;"> <span style="font-weight: 400;">as</span> ${movie.credits.cast[i.toString()].character}</h5>
            <button class="btn btn-outline-warning btn-md" type="button" onclick="getActor(${movie.credits.cast[i.toString()].id})">Learn more</button>                         
          </div>
        </div>
      </div>
    </div>`);
    } else continue;
  }

  getReviews(id, 1);


}

async function getReviews(id, page) {
  if (page === undefined) {
    page = 1;
  }
  const responseRating = await fetch(`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=4545f05e9a363270fa164a4080bb8948&page=${page}`);
  const reviews = await responseRating.json();

  $('ul, .scrolling-wrapper').empty();
  renderReviews(reviews, page);

}

function renderReviews(reviews, page) {

  for (item of reviews.results) {
    $('.scrolling-wrapper').append(`
    <div class="card shadow mx-2 mb-4">
      <div class="card-body">
         <h4 class="card-title px-2">${item.author}</h4>
         <p class="card-text px-2">"${item.content}..."<br/><br/> </p>
         <hr/>
         <a href="${item.url}" class="card-link">Read more</a>
      </div>
    </div>

`);
  }
}

async function getActor(id) {

  $('body').empty();
  $('body').append(`<div class="spinner-border" style="top:50%; left:50%;display: block; position: fixed; z-index: 1031;" role="status">
  <span class="sr-only" style="top:50%; left:50%;"></span>
</div>`);
  console.log(id);
  const responseActor = await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=4545f05e9a363270fa164a4080bb8948`);
  const actor = await responseActor.json();
  var deathday = actor.deathday;
  if (deathday !== null) {
    deathday = " - " + deathday;
  } else deathday = "";
  const responseRelateInfo = await fetch(`https://api.themoviedb.org/3/discover/movie?api_key=4545f05e9a363270fa164a4080bb8948&with_people=${id}`);
  const info = await responseRelateInfo.json();
  console.log(info.results['0'].title);
  var relatedMovies = info.results;

  $(window).scrollTop(0);
  $('body').empty();
  $('body').append(`<div class="container col-md-8 mb-4" style="height:93vh">

  <div class="row col-md-12 justify-content-center">
    <nav class="navbar navbar-light  my-2 col-md-12 my-2"  >
    <a class="btn btn-warning" onclick="reloadPage()">Home</a>
    <form class="form-inline" id='searchForm' submit="search(event, 1)" action="#">
      <input class="form-control mr-sm-1" id='searchbox' type="search" placeholder="Search" aria-label="Search">
      <select class="form-control form-control-md mr-sm-1" id="search-mode">
        <option>Movie name</option>
        <option>Actor's name</option>
      </select>
      <button class="btn btn-warning my-2 my-sm-0" type="submit"
        onclick="getQuery(event, 1); return false;">Search</button>
    </form>
  </nav>
  </div>
  
  <h1 class="text-center my-5">Overview</h1>

  <div class="row mt-3" style="height:70vh;">
  
      <div class="card col-md-3 h-100 shadow-lg w-100" >
          <img src="https://image.tmdb.org/t/p/w1280/${actor.profile_path}" class="card-img-top img-fluid shadow-lg" alt="">
          <div class="card-body " style="text-align: center;">
              <h2 style="font-weight: 700;">${actor.name}</h2>
              <h6 style="font-weight: 100;">${actor.birthday}${deathday}</h6>
              <h6>${actor.place_of_birth}</h6>
              <h3 style="font-weight: 200;   "><span style="font-weight: 800; color:rgb(255, 166, 0);">${actor.popularity}</span> popularity</h3>
          </div>
      </div>
      <div class="col-md-9 h-100 w-100 ">
          <div class="carousel slide h-100 w-100" id="carousel-moviesbyactor">
              <ol class="carousel-indicators" >
                  
              </ol>
              <div class="carousel-inner h-100 shadow-lg w-100  ">
                  
              </div>
              <a class="carousel-control-prev" href="#carousel-moviesbyactor" role="button" data-slide="prev" >
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-moviesbyactor" role="button" data-slide="next" >
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>
          </div>
      </div>
  </div>
</div>

<h1 class="text-center mb-5">Biography</h1>

<div class="shadow-lg container card col-md-8 mb-5 px-5 py-5" style="line-height: 2; text-align: justify; justify-content: center; align-content: center;">
 
  <p class="px-5 py-5">${actor.biography}</p>
</div>    
`);

  for (let i = 0; i < relatedMovies.length; i++) {

    console.log(relatedMovies[i.toString()]);
    $('.carousel-indicators').append(`<li data-target="#carousel-moviesbyactor" data-slide-to=${i.toString()} class="active"></li>`);
    if (i == 0) {
      $('.carousel-inner').append(`<div class="carousel-item active h-100">
    <img src="https://image.tmdb.org/t/p/w1280${relatedMovies[i.toString()].backdrop_path}" class="h-100" alt="">
    <div class="carousel-caption d-none d-md-block" style="text-align: center;">
            <h1>${relatedMovies[i.toString()].title}</h1>
            <h3> <span style="font-weight: 800;">${relatedMovies[i.toString()].release_date}</span></h3>
            <p style="text-align: justify;">${relatedMovies[i.toString()].overview}</p>
            <button class="btn btn-outline-warning btn-sm mb-3" onclick="getDetails(${relatedMovies[i.toString()].id})">More info</button>
    </div>
    </div>`)

    } else {
      $('.carousel-inner').append(`<div class="carousel-item h-100">
    <img src="https://image.tmdb.org/t/p/w1280${relatedMovies[i.toString()].backdrop_path}" class="h-100" alt="">
    <div class="carousel-caption d-none d-md-block" style="text-align: center;">
            <h1>${relatedMovies[i.toString()].title}</h1>
            <h3><span style="font-weight: 800;">${relatedMovies[i.toString()].release_date}</span></h3>
            <p style="text-align: justify;">${relatedMovies[i.toString()].overview}</p>
            <button class="btn btn-outline-warning btn-sm mb-3" onclick="getDetails(${relatedMovies[i.toString()].id})">More info</button>
    </div>
    </div>`)
    }
  }
}
function reloadPage(){
  location.reload(true);
}